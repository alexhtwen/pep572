# Example 1
# 設定「人數」。真正的系統這些數字會從外界(例如檔案、資料庫、API)取得。
import time
sign_ins = int(input('自行報名人數：'))
wild_cards = int(input('持外卡人數：'))


def show_msg1(n):
    print(f"網路爬蟲班有 {n} 人參加，太多了，得篩選學生才行。")


def show_msg2(n):
    print(f"網路爬蟲班 {n} 人參加，要分為兩班。")


def show_msg3(n):
    print(f"網路爬蟲班 {n} 人參加，確定開班。")


def show_msg4(n):
    print(f"網路爬蟲班只有 {n} 人參加，未達開班標準，要再加油招生。")

# 以前我都喜歡用doATimeConsumingTask這種命名方式(camel casing)。


def do_a_time_consuming_task(sign_ins=0, wild_cards=0):
    time.sleep(5)  # 用sleep n seconds來模擬某件time-consuming(耗時)的工作。
    return sign_ins + wild_cards  # total參加人數 = 自行報名人數 + 持外卡者(特權分子)人數


def show_consuming_time(time1, time2):
    print(f"耗時： {(time2 - time1):2.2f} 秒")

# 最糟的寫法：只為省一行程式碼而讓程式拖慢好幾倍。通常是程式新手犯的錯，不過有時老手也會不小心...


def tortoise_run(sign_ins=0, wild_cards=0):
    if do_a_time_consuming_task(sign_ins, wild_cards) > 100:
        show_msg1(do_a_time_consuming_task(sign_ins, wild_cards))    # 呼叫耗時函數2次
    elif do_a_time_consuming_task(sign_ins, wild_cards) > 50:
        show_msg2(do_a_time_consuming_task(sign_ins, wild_cards))    # 呼叫耗時函數3次
    elif do_a_time_consuming_task(sign_ins, wild_cards) >= 20:
        show_msg3(do_a_time_consuming_task(sign_ins, wild_cards))    # 呼叫耗時函數4次
    else:
        show_msg4(do_a_time_consuming_task(sign_ins, wild_cards))    # 呼叫耗時函數4次


# time1 = time.time()
# tortoise_run(sign_ins, wild_cards)     # 這程式跑得很慢，是「龜兔賽跑」中的烏龜。
# time2 = time.time()

# show_consuming_time(time1, time2)


def hare_run(total_students=0):
    if total_students > 100:   # 永遠只呼叫1次耗時函數
        show_msg1(total_students)
    elif total_students > 50:
        show_msg2(total_students)
    elif total_students >= 20:
        show_msg3(total_students)
    else:
        show_msg4(total_students)


# # 改進-1：先用變數接值，這是一般的assignment operator。

# time1 = time.time()
# num_students = do_a_time_consuming_task(
#     sign_ins, wild_cards)   # 用變數接值(assignment operator)
# hare_run(num_students)     # 這程式跑得很快，屬於「龜兔賽跑」中的兔子。
# time2 = time.time()

# show_consuming_time(time1, time2)


def walrus_run(sign_ins=0, wild_cards=0):
    if (total_students := do_a_time_consuming_task(sign_ins, wild_cards)) > 100:  # assignment expression
        show_msg1(total_students)   # 永遠只呼叫1次耗時函數
    elif total_students > 50:
        show_msg2(total_students)
    elif total_students >= 20:
        show_msg3(total_students)
    else:
        show_msg4(total_students)


# 改進-2：用assignment expression：

time1 = time.time()
walrus_run(sign_ins, wild_cards)    # 改用海象運算。
time2 = time.time()

show_consuming_time(time1, time2)
