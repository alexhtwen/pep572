# Example 4
def get_discounted_prices(price):
    new_price = round(price * 0.9)
    print('Lets get_discounted_prices()', new_price)
    return new_price


prices = [500, 120, 49, 110, 99, 112, 106, 109, 111, 104]

# 這是 list comprehension.
# 從資料庫讀出每項商品的價格，透過get_discounted_prices()函數計算
# 這些商品折扣後的價格，
# 這個函數傳回的是單一商品打折後的價格。
# 再將打折後價格 >= 100元的部分抽出，另存到discounted_prices變數中。
# 這段code的缺點就是get_discounted_prices(price)寫了兩次，如果get_discounted_prices(price)是個非常耗時的
# 函數，程式效能就不理想了。
# list comprehension (串列生成式)
# discounted_prices = \
#     [get_discounted_prices(price)
#      for price in prices if get_discounted_prices(price) >= 100]
# print(discounted_prices)
# exit(0)

# 改用assignment expression，只須一次
# get_discounted_prices()
discounted_prices = \
    [new_price for price in prices
     if (new_price := get_discounted_prices(price)) >= 100]
print(discounted_prices)
