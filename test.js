// JavaScript is NOT Java

// 1) 變數(variable) "要" 先 "宣告" 再使用
//    變數可以不先"宣告"馬上賦值
//    宣告變數的方法:

// 1-1) var (舊版)
var a, b
a = 3       // number (賦值，初值設定)
b = '3'     // str
a = 5       // 賦值, 非初值設定









// 1-2) let (新版)
let x, y, z

// 1-3) const (新版)
const r, s, t





console.log(a)