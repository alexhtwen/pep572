# Example 3
# 下面程式中的readline()函數必須寫2次。
# with open('./test.txt', 'r') as f:
#     line = f.readline()
#     # 第1次的readline()，作用是讓下面的while loop一開始就有值
#     # 可以判斷。
#     while line:
#         print(line, end='')
#         line = f.readline()   # 第2次的readline()
# exit(0)

# 改用無窮迴圈(while True就是無窮迴圈的寫法)，再在迴圈內判斷，
# 符合某個條件就跳離，這種寫法術語稱為loop and a half，
# 即「一個半迴圈」。這樣可以避免寫兩次readline()。
# with open('./test.txt', 'r') as f:
#     while True:
#         line = f.readline()   # 只有一次readline()。
#         if not line:   # 空字串
#             break
#         print(line, end='')
# exit(0)

# 如果覺得寫兩次readline()身段不夠漂亮，又不想用loop and a half，
# 那麼assignment expression就是首選。
# 程式碼相對簡潔優雅。至於是否較易閱讀和理解，就見仁見智了。
# 就因為見仁見智，所以才吵翻天，最後讓BDFL下台一鞠躬。
with open('./test.txt', 'r') as f:
    while line := f.readline():  # 這列動作： 1)讀取一行文字並存入line； 2)判斷line是否為空字串。
        print(line, end='')
