# 討論：為甚麼不直接拿assignment operator('=')使用，
#       而要新創一個walrus operator(':=')?
def f(a, b, /, c, d, *, e, f):
    print(a, b, c, d, e, f)


# f(1, 2, 3, 5, 6, f=9)         # invalid call
# f(10, 20, 30, d=40, e=50, f=60)   # valid call

# f(10, b=20, c=30, d=40, e=50, f=60)   # b cannot be a keyword argument
f(10, 20, 30, 40, e=50, f=60)           # e must
