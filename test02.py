# Example 2 (另一個不當節省一行程式碼的例子)
import re   # regular expression
data = "K1075638921256  "
pattern = '[A-Z]{1}[1-2]{1}[0-9]{8}'   # 中華民國國民身分證編號的pattern


# bad practice
# Guido found several examples where a programmer repeated a subexpression,
# slowing down the program, in order to save just one line of code.
# if re.match(pattern, data):   # re.match()可能是非常耗時的工作唷。
#     group = re.match(pattern, data).group()   # repeats the re.match()
#     print(group)
# else:
#     group = 'No such group.'


# good practice using one more line of code.
# my_match = re.match(pattern, data)  # this is a statement.
# if my_match:
#     group = my_match.group()   # no repeats
# else:
#     group = 'No such group.'
# print(group)
# exit(0)

# good practice using walrus operator(assignment expression).
if my_match := re.match(pattern, data):
    group = my_match.group()   # no repeats
else:
    group = 'No such group.'
print(group)
